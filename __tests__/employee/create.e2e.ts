import 'jest-extended';

import { SetupRequest, IHeaders } from '../../src/utils/testing';
const callAPI = new SetupRequest().request;

beforeAll(async () => {
  // This hook excutes before run test cases
  console.log('beforeAll');
  
});

describe('Create Employee', () => {
  it('Return message Create Employee successfully', async () => {
    const result = await callAPI({ method: 'post', url: '/employees', data: {
      "name": "Liem Test"
    } });
    console.log('result', result);
    
    expect(result.response).toMatchObject({msg: 'Create Employee Sucessfully'}); 
  });
});
