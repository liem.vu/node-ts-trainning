class HttpException extends Error {
  httpStatus: number;
  errorCode: string;
  message: string;
  constructor(httpStatus: number, errCode: string, message: string) {
    super(message);
    this.httpStatus = httpStatus;
    this.errorCode = errCode;
    this.message = message;
  }
}

export default HttpException;
