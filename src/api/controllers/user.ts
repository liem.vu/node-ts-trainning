import { NextFunction, Request, Response } from 'express';
import { Container } from 'typedi';
import { UserService } from '../../services/user';
import CustomError from '../../utils/customError';

/**
 * @class UserController
 */
export class UserController {
 
  public async getListUsers(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
    const userService = Container.get(UserService);
    const listUser = await userService.getUsers();
    return res.send(listUser)
  }

  public async register(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
    const userService = Container.get(UserService);
    await userService.register(req.body);
    return res.status(200).json({
      msg: 'register successfully'
    });
  }

  public async syncFirebaseAuthUser(req: Request, res: Response, next: NextFunction) {
    try {
      const service = Container.get(UserService);
      await service.syncFirebaseAuthUser(req.body);
      res.status(204).json();
    } catch (e) {
      if (e instanceof CustomError) {
        const { httpStatus, errorCode, message } = e;
        res.status(httpStatus || 400).send({ errorCode, message });
      } else {
        return next(e);
      }
    }
  }

  public async testFcm(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
    const userService = Container.get(UserService);
    await userService.testMsg();
    return res.send({msg: 'Send successfully'})
  }
}
