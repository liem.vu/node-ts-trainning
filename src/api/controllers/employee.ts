import { NextFunction, Request, Response } from 'express';
import { Container } from 'typedi';
import { EmployeeService } from '../../services/employee';
import CustomError from '../../utils/customError';


/**
 * @class EmployeeController
 */
export class EmployeeController {
  public async seed(req: Request, res: Response) {
    const employeeService = Container.get(EmployeeService);
    await employeeService.seed();
    return res.send({ msg: 'Seed Succesfully' });
  }

  public async getEmployeeById(req: Request, res: Response, next: NextFunction) {
    try {
      const employeeService = Container.get(EmployeeService);
      const id = Number(req.params.id)
      
      const employee = await employeeService.getEmployeeById(id);
      
      return res.send(employee);
    } catch (error) {
      if (error instanceof CustomError) {
        const { httpStatus, errorCode, message } = error;
        res.status(httpStatus || 400).send({ errorCode, message });
      } else {
        return next(error);
      }
    }

    
  }

  public async getListEmployees(req: Request, res: Response) {
    const employeeService = Container.get(EmployeeService);
    const employees = await employeeService.getListEmployees(req.query);
    return res.send(employees);
  }

  public async createNewEmployee(req: Request, res: Response) {
    const employeeService = Container.get(EmployeeService);
    await employeeService.saveEmployee(req.body);
    return res.send({msg: 'Create Employee Sucessfully'}).status(201);
  }

  public async updateEmployee(req: Request, res: Response) {
    const employeeService = Container.get(EmployeeService);
    await employeeService.saveEmployee(req.body);
    return res.send({msg: 'Update Employee Sucessfully'});
  }

  public async deleteEmployee(req: Request, res: Response) {
    const employeeService = Container.get(EmployeeService);
    const id = Number(req.params.id);
    await employeeService.deleteEmployeeById(id);
    return res.send({msg: 'Delete Employee Sucessfully'});
  }
}
