import Joi from 'joi';
import { joiErrorHandler } from 'validator-module';
import { BaseFilterParamSchema } from './base';

export const InsertUserSchema = Joi.object({
    fullname: Joi.string().required().error(joiErrorHandler()),
    username: Joi.string().required().error(joiErrorHandler()),
    password: Joi.string().required().error(joiErrorHandler()),
    email: Joi.string().required().email().error(joiErrorHandler()),
    phone: Joi.string().required().error(joiErrorHandler()),
    dial_code: Joi.string().required().error(joiErrorHandler()),
});
