import Joi from 'joi';
import { joiErrorHandler } from 'validator-module';
import { BaseFilterParamSchema } from './base';

export const InsertEmployeeSchema = Joi.object({
    name: Joi.string().required().error(joiErrorHandler()),
    manager_id: Joi.number().error(joiErrorHandler()),
});

export const UpdateEmployeeSchema = InsertEmployeeSchema.append({
    id: Joi.number().required().error(joiErrorHandler())
})

export const GetListEmployeeSchema = BaseFilterParamSchema;