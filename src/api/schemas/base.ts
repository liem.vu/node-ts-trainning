import Joi from 'joi';
import { joiErrorHandler, ValidatorUtil } from 'validator-module';

import appConstant from '../../utils/appConstant';

export const BaseFilterParamSchema = Joi.object({
  filter: Joi.string().min(1).max(100).error(joiErrorHandler()),

  limit: Joi.number().integer().min(1).max(100).default(appConstant.DEFAULT_PAGINATE.MAX).error(joiErrorHandler()),

  page: Joi.number().integer().min(1).default(appConstant.DEFAULT_PAGINATE.SKIP).error(joiErrorHandler()),

  sorting: Joi.string()
    .regex(/(?<column>[a-z]+(_[a-z]+)?)\s(?<dir>(asc|desc))$/)
    .default('created_at desc')
    .error(joiErrorHandler()),
});

export const removeDuplicate = ValidatorUtil.removeDuplicate;

export const DateValidation = ValidatorUtil.DateValidation;

export const DateFilterParamsSchema = {
  date_from: Joi.date().iso().error(joiErrorHandler()),
  date_to: Joi.date().iso().error(joiErrorHandler()),
};
