import { Request, Response, NextFunction } from 'express';
import HttpException from '../exceptions/httpException';
import logger from '../../loaders/logger';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function errorHandler(error: any, _request: Request, response: Response, _next: NextFunction) {
  if (error instanceof HttpException) {
    const status = error.httpStatus;
    const errorCode = error.errorCode;
    const message = error.message;
    response.status(status).send({ errorCode, message });
  } else {
    logger.error(`🔥Server internal error. Message: ${error}. \n Stack: ${error.stack}`);
    response.status(500).send({ status: 'UNHANDLED_ERROR', message: 'Server internal error!' });
  }
}

export default errorHandler;
