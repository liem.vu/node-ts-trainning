'use strict';

/**
 * Module dependencies.
 */
import { InvalidArgumentError, AbstractGrantType, InvalidRequestError, InvalidGrantError } from 'oauth2-server';
import util from 'util';

/**
 * Constructor.
 */

function FirebaseTokenGrantType(options) {
  options = options || {};
  console.log('liem|FirebaseTokenGrantType');
  

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  if (!options.model.getUserByFirebaseToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `getUserByFirebaseToken()`');
  }

  if (!options.model.saveToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
  }

  AbstractGrantType.call(this, options);
}

/**
 * Inherit prototype.
 */

util.inherits(FirebaseTokenGrantType, AbstractGrantType);

/**
 * Retrieve the user from the model using a username/password combination.
 *
 * @see https://tools.ietf.org/html/rfc6749#section-4.3.2
 */

FirebaseTokenGrantType.prototype.handle = async function (request, client) {
  console.log('liem|handle');
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  const scope = this.getScope(request);
  const user = await this.getUserByFirebaseToken(request);
  const result = await this.saveToken(user, client, scope);

  return result;
};

/**
 * Get user using a username/password combination.
 */

FirebaseTokenGrantType.prototype.getUserByFirebaseToken = async function (request) {
  console.log('liem|getUserByFirebaseToken');
  const { firebase_token } = request.body;
  if (!firebase_token) {
    throw new InvalidRequestError('Missing parameter: `firebase_token`');
  }

  const user = await this.model.getUserByFirebaseToken.call(this.model, firebase_token);

  if (!user) {
    throw new InvalidGrantError('Invalid grant: Firebase credentials are invalid');
  }

  return user;
};

/**
 * Save token.
 */

FirebaseTokenGrantType.prototype.saveToken = async function (user, client, scope) {
  console.log('liem vao day');
  
  const fns = [
    this.validateScope(user, client, scope),
    await this.generateAccessToken(client, user, scope),
    await this.generateRefreshToken(client, user, scope),
    this.getAccessTokenExpiresAt(),
    this.getRefreshTokenExpiresAt(),
  ];

  const [_scope, accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt] = await Promise.all(fns);
  const token = {
    accessToken: accessToken,
    accessTokenExpiresAt: accessTokenExpiresAt,
    refreshToken: refreshToken,
    refreshTokenExpiresAt: refreshTokenExpiresAt,
    scope: _scope,
  };

  return await this.model.saveToken.call(this.model, token, client, user);
};

/**
 * Export constructor.
 */

export default FirebaseTokenGrantType;
