import { Container } from 'typedi';
import { NextFunction, Request, Response } from 'express';
import OAuthServer, { Request as OAuthRequest, Response as OAuthResponse } from 'oauth2-server';
import OAuthError from 'oauth2-server/lib/errors/oauth-error';

import AuthService from '../../services/auth';

import AppConstant from '../../utils/appConstant';
import { convertOAuthError } from '../../utils/authErrorUtils';

import HttpException from '../exceptions/httpException';
import FirebaseTokenGrantType from './firebaseTokenGrant';

class AuthMiddleware {
  public async authenticate(req: Request, res: Response, next: NextFunction) {
    const oauthServer = new OAuthServer({
      model: Container.get(AuthService),
    });

    return oauthServer
      .authenticate(new OAuthRequest(req), new OAuthResponse(res), {
        scope: 'true',
      })
      .then(token => {
        res.locals.token = token;
        next();
      })
      .catch(err => {
        console.log(err);
        
        if (err instanceof OAuthError) {
          const convertedErr = convertOAuthError(err, req.header('locale'));
          next(
            new HttpException(
              convertedErr.code || 500,
              convertedErr.name ? convertedErr.name.toUpperCase() : 'OAUTHERROR',
              convertedErr.message || 'Internal Server Error',
            ),
          );
        } else {
          next(err);
        }
      });
  }

  public async token(req: Request, res: Response, next: NextFunction) {
    const oauthServer = new OAuthServer({
      model: Container.get(AuthService),
    });
    console.log('tess', req.body);
    console.log(AppConstant.ACCESS_TOKEN_LIFETIME);
    

    return oauthServer
      .token(new OAuthRequest(req), new OAuthResponse(res), {
        accessTokenLifetime: AppConstant.ACCESS_TOKEN_LIFETIME,
        refreshTokenLifetime: AppConstant.REFRESH_TOKEN_LIFETIME,
        scope: req.body.scope,
      })
      .then(token => {
        res.locals.token = token;
        next();
      })
      .catch(err => {
        console.log(err);
        
        if (err instanceof OAuthError) {
          const convertedErr = convertOAuthError(err, req.header('locale'));
          next(
            new HttpException(
              convertedErr.code || 500,
              convertedErr.name ? convertedErr.name.toUpperCase() : 'OAUTHERROR',
              convertedErr.message || 'Internal Server Error',
            ),
          );
        } else {
          next(err);
        }
      });
  }


  public async firebaseToken(req: Request, res: Response, next: NextFunction) {
    const oauthServer = new OAuthServer({
      model: Container.get(AuthService),
    });

    return oauthServer
      .token(new OAuthRequest(req), new OAuthResponse(res), {
        accessTokenLifetime: AppConstant.ACCESS_TOKEN_LIFETIME,
        refreshTokenLifetime: AppConstant.REFRESH_TOKEN_LIFETIME,
        scope: req.body.scope,
        extendedGrantTypes: {
          firebase_auth: FirebaseTokenGrantType,
        },
      })
      .then(token => {
        res.locals.token = token;
        next();
      })
      .catch(err => {
        if (err instanceof OAuthError) {
          const convertedErr = convertOAuthError(err, req.header('locale'));
          next(
            new HttpException(
              convertedErr.code || 500,
              convertedErr.name ? convertedErr.name.toUpperCase() : 'OAUTHERROR',
              convertedErr.message || 'Internal Server Error',
            ),
          );
        } else {
          next(err);
        }
      });
  }
}

export default new AuthMiddleware();
