import { Validator } from 'validator-module';
import Joi from 'joi';
import { RequestHandler } from 'express';

import { localeField, validateMessage } from '../../utils/localeUtils';

import HttpException from '../exceptions/httpException';

export function joiValidationMiddleware(schema: Joi.Schema, select: string): RequestHandler {
  return (req, res, next) => {
    const validator = new Validator({ _fieldCallback: localeField, _errorCallback: validateMessage });
    const result = validator.validate(schema, req[select], req.header('locale'));
    if (result.error) {
      next(new HttpException(400, 'DATA_INVALID', result.message));
    } else {
      req[select] = result.data.__group || result.data;
      next();
    }
  };
}
