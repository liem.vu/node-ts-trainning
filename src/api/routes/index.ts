import { Request, Response, Router } from 'express';
import AppConstant from '../../utils/appConstant';

import userRoute from './user';
import employeeRoute from './employee';

export default () => {
  const app = Router();
  const { AUTH_API, VERSION } = AppConstant.API_REF;
  const apiPrefix = AUTH_API + VERSION;

  userRoute(app, apiPrefix);
  employeeRoute(app, apiPrefix);

  app.get('/check', (req: Request, res: Response) => {

    return res.send({ msg: 'Hello Liem' })
  });

  return app;
};
