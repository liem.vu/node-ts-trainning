import { Router } from 'express';
import { EmployeeController } from '../../controllers/employee';
import { joiValidationMiddleware } from '../../middlewares/validatorMiddleware';
import { GetListEmployeeSchema, InsertEmployeeSchema, UpdateEmployeeSchema } from '../../schemas/employee';


const router = Router();

export default (app: Router, prefix: string): void => {
  const employeeController = new EmployeeController();

  app.use(prefix + '/employees', router);
  

  router.get('/:id', employeeController.getEmployeeById);

  router.get('/?', joiValidationMiddleware(GetListEmployeeSchema, 'query'), employeeController.getListEmployees);

  router.post(
    '/?',
    joiValidationMiddleware(InsertEmployeeSchema, 'body'),
    employeeController.createNewEmployee
  );

  router.put(
    '/?',
    joiValidationMiddleware(UpdateEmployeeSchema, 'body'),
    employeeController.updateEmployee
  );

  router.delete('/:id([0-9]+)/?',  employeeController.deleteEmployee);

};
