import { Request, Response, Router } from 'express';
import { UserController } from '../../controllers/user';
import { joiValidationMiddleware } from '../../middlewares/validatorMiddleware';
import { InsertUserSchema } from '../../schemas/user';
import authMiddleware from '../../middlewares/auth';

const router = Router();

export default (app: Router, prefix: string): void => {
  const userController = new UserController();
  console.log(prefix);
  
  app.use(prefix + '/users', router);

  router.get(
    '/?',
    userController.getListUsers,
  );

  router.post(
    '/register',
    joiValidationMiddleware(InsertUserSchema, 'body'),
    userController.register,
  );

  router.put(
    '/sync-firebase-auth',
    userController.syncFirebaseAuthUser,
  );

  router.get(
    '/testfcm',
    userController.testFcm,
  );

  router.post('/token/?', authMiddleware.token, (req: Request, res: Response)=> {
    res.status(200).json(res.locals.token);
  });

  router.get(
    '/authenticate/?',
    authMiddleware.authenticate,
    (req: Request, res: Response)=> {
      res.status(200).json(res.locals.token);
    },
  );

  router.post('/firebase-token', authMiddleware.firebaseToken, (req: Request, res: Response)=> {
    res.status(200).json(res.locals.token);
  });
};

