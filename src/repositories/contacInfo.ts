import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { ContacInfo } from '../entity/ContacInfo';

@Service()
@EntityRepository(ContacInfo)
export class ContactInfoRepository extends Repository<ContacInfo> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
