import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { User } from '../entity/User';
import { InjectConnection } from 'typeorm-typedi-extensions';

@Service()
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
