import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { Employee } from '../entity/Employee';
import { plainToClass } from 'class-transformer'
import { IEmployee, IEmployeeQuery } from '../interfaces/IEmployee';
import { buildQueryBuilderPaging, buildQueryBuilderSorting } from '../utils/conditionBuilder';
import { extractSorting } from '../utils/commonUtils';

@Service()
@EntityRepository(Employee)
export class EmployeeRepository extends Repository<Employee> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }

  async getEmployeeById(id: number) {
    const employee = await this.createQueryBuilder('employee')
      .leftJoinAndSelect('employee.directReports', 'directReports')
      .leftJoinAndSelect('employee.meetings', 'meetings')
      .leftJoinAndSelect('employee.tasks', 'tasks')
      .where('employee.id = :employeeId', { employeeId: id })
      .getOne();

    return plainToClass(Employee,employee);
  }

  async getListEmployees(params: IEmployeeQuery): Promise<[IEmployee[], number]>  {
    const { filter, sorting } = params;
    
    const query = await this.createQueryBuilder('employee');
    if (filter) {
      const _filter =  `%${filter}%`;
      console.log('emm', _filter);
      query.where("employee.name like :filter", {
        filter:_filter
      });
    }

    buildQueryBuilderPaging(params, query);
    console.log('sorting', sorting);
    
    if (sorting) {
      buildQueryBuilderSorting(extractSorting(sorting), query, 'employee');
    }

    const [employees, count] = await query.getManyAndCount();
    return [plainToClass(Employee, employees), count];
  }
}
