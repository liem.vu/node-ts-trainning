import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { Task } from '../entity/Task';

@Service()
@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
