import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { Meeting } from '../entity/Meeting';

@Service()
@EntityRepository(Meeting)
export class MeetingRepository extends Repository<Meeting> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
