import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { Client } from '../entity/Client';

@Service()
@EntityRepository(Client)
export class ClientRepository extends Repository<Client> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
