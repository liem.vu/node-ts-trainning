import { Service } from 'typedi';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { InjectConnection } from 'typeorm-typedi-extensions';
import { Token } from '../entity/Token';

@Service()
@EntityRepository(Token)
export class TokenRepository extends Repository<Token> {
  constructor(@InjectConnection() private readonly connection: Connection) {
    super();
  }
}
