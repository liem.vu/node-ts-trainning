/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console*/
const io = require('socket.io-client');
console.log('test');

const socket = io('http://localhost:3000/user-service/socket/notification');
socket.on('connect', () => {
  console.log('ping');
  socket.emit('authentication', {
    access_token:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Mjg0MzU3MjgsImV4cCI6MTYyODQzNzUyOCwidWlkIjoxLCJjbGFpbXMiOnsidXNlcl9pZCI6MSwidXNlcm5hbWUiOiJsaWVtdmgiLCJjbGllbnRfaWQiOiJtYXljdWFsaWVtIn0sInNjb3BlIjoiVXNlclNlcnZpY2UifQ.HmwX-mZyrIKrUvyQLPif7Son9tsM1DC-RQxYPu4emuE',
  });
  socket.emit('message:notification', {
    a: 'hello world',
    recipient_id: '9e854a02-f61d-4c73-8905-8d165147aab4',
  });
});
socket.on('connect_error', err => {
  console.log(`connect_error due to ${err}`);
});
socket.on('authenticated', result => {
  console.log('authenticated', result);
});
socket.on('unauthorized', result => {
  console.log('unauthorized', result);
});

socket.on('message:notification', result => {
  console.log(socket.id);
  console.log('receive', result);
});
