/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console*/
const io = require('socket.io-client');
console.log('test');

const socket = io('http://localhost:3000/user-service/socket/notification');
socket.on('connect', () => {
  console.log('ping');
  socket.emit('authentication', {
    access_token:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Mjc4MTQyNzgsImV4cCI6MTYyNzgxNjA3OCwidWlkIjoiYjdhMDA0NTAtMzUxZC00MGM1LTgyYjgtYWUzYTk3YzExMDI1IiwiY2xhaW1zIjp7InVzZXJfaWQiOiJiN2EwMDQ1MC0zNTFkLTQwYzUtODJiOC1hZTNhOTdjMTEwMjUiLCJ1c2VybmFtZSI6Im9wZXJhdG9yIiwiY2xpZW50X2lkIjoibWF5Y3VhdGFuIn0sInNjb3BlIjoiVXNlclNlcnZpY2UifQ.SwShL5eFk4tXvAefdbK8kl76CUXaGv5Qi6F5LlJ1jhk',
  });
  socket.emit('message:notification', {
    a: 'hello world',
    recipient_id: '9e854a02-f61d-4c73-8905-8d165147aab4',
  });
});
socket.on('connect_error', err => {
  console.log(`connect_error due to ${err.message}`);
});
socket.on('authenticated', result => {
  console.log('authenticated', result);
});
socket.on('unauthorized', result => {
  console.log('unauthorized', result);
});

socket.on('message:notification', result => {
  console.log(socket.id);
  console.log('receive', result);
});
