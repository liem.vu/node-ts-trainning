import { InjectRepository } from 'typeorm-typedi-extensions';
import { Inject, Service } from 'typedi';
import { UserRepository } from '../repositories/user';
import { User } from '../entity/User';
import { IUser } from '../interfaces/IUser';
import CustomError from '../utils/customError';
import { getPhoneNumberWithDialCode, hashPassword } from '../utils/commonUtils';
import NotificationEmitter from '../subscribers/notification';
import FirebaseAuthService from './firebaseAuth';
@Service()
export class UserService {
  constructor() {
  }
  @Inject('logger') private readonly logger;
  @InjectRepository() protected readonly userRepository: UserRepository;
  @Inject() protected readonly firebaseAuth: FirebaseAuthService;

  async getUsers() {
    return await this.userRepository.find({});
  }

  public async getUserById(id: string): Promise<IUser> {
    return this.userRepository.findOne(id);
  }

  async register(input: IUser) {
    const dupUsername = await this.userRepository.findOne({ username: input.username });
    if (dupUsername) {
      await this.firebaseAuth.updateUser(dupUsername.id ,{
        email: dupUsername.email,
        phoneNumber: '+84987095270'
      })
      return dupUsername;
    }
    input.password = await hashPassword(input.password);
    const user = new User(input);
    const saveUser = await this.userRepository.save(user);
    // NotificationEmitter.emit('push_notification_socket', input); 
    if (saveUser) {
      await this.firebaseAuth.createUser({
        uid: saveUser.id,
        email: saveUser.email,
        phoneNumber: getPhoneNumberWithDialCode(saveUser.phone, saveUser.dial_code),
      });
      
    }        
    return saveUser;
  }

  public async syncFirebaseAuthUser(params: { phone: string }): Promise<void> {
    const uidFirebase = await this.firebaseAuth.getUserByPhone(params.phone)
    const user = await this.userRepository.findOne({ id: uidFirebase });
    console.log(user);
    
    if (!user) {
      console.log('Doing');
      
    }
    
    return;
  }

  async testMsg() {
    NotificationEmitter.emit('push_notification', {
      title: 'Hi Liem',
      content: 'content',
      recipient_id: 'e00600f7-01f6-4180-8bd7-252a8a33a4ba',
      data: {
        id: '123'
      }
    });  
    return null;
  }
}
