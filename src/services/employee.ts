import { InjectRepository } from 'typeorm-typedi-extensions';
import { Inject, Service } from 'typedi';
import { EmployeeRepository } from '../repositories/employee';
import { ContactInfoRepository } from '../repositories/contacInfo';
import { TaskRepository } from '../repositories/task';
import { MeetingRepository } from '../repositories/meeting';
import CustomError from '../utils/customError';
import { employeeMessage } from '../utils/localeUtils';
import { wrapPagination } from '../utils/commonUtils';
import { IEmployee, IEmployeeQuery } from '../interfaces/IEmployee';
import { Employee } from '../entity/Employee';

@Service()
export class EmployeeService {
  constructor() {
  }
  @Inject('logger') private readonly logger;
  @InjectRepository() protected readonly employeeRepository: EmployeeRepository;
  @InjectRepository() protected readonly contactInfoRepository: ContactInfoRepository;
  @InjectRepository() protected readonly taskRepository: TaskRepository;
  @InjectRepository() protected readonly meetingRepository: MeetingRepository;

  async seed() {
    const ceo = this.employeeRepository.create({ name: 'Mr. CEO' });
    await this.employeeRepository.save(ceo);

    const ceoContactInfo = this.contactInfoRepository.create({
      email: 'ceo@email.com'
    });

    ceoContactInfo.employee = ceo;
    await this.contactInfoRepository.save(ceoContactInfo);

    const manager = this.employeeRepository.create({
      name: 'Hoang Liem',
      manager: ceo
    });

    const task1 = this.taskRepository.create({ name: 'Task 1' });
    this.taskRepository.save(task1);

    const task2 = this.taskRepository.create({ name: 'Task 2' });
    this.taskRepository.save(task2);

    manager.tasks = [task1, task2];

    const meeting1 = this.meetingRepository.create({ zoomUrl: 'meeting1.com' });
    meeting1.attendees = [ceo];
    await this.meetingRepository.save(meeting1);

    manager.meetings = [meeting1];

    await this.employeeRepository.save(manager);
  }

  async getEmployeeById(id: number, lang?: string): Promise<IEmployee> {
    try {
      const employee = await this.employeeRepository.getEmployeeById(id);
      
      if (!employee) {
        throw new CustomError(404, 'NOT_FOUND', employeeMessage(lang, 'NOT_FOUND'));
      }

      return employee;
    } catch (error) {
      
      throw new CustomError(404, 'NOT_FOUND', employeeMessage(lang, 'NOT_FOUND'));
    }
  }

  async getListEmployees(params: IEmployeeQuery) {
    const [data, count] = await this.employeeRepository.getListEmployees(params);

    return wrapPagination(data, count, params);
  }

  async saveEmployee(input: any) {
    const user = new Employee(input);
    console.log(input);
    
    if (input.manager_id) {
      user.manager = await this.employeeRepository.findOne({id: input.manager_id});
      console.log(user);
      
    }
    return await this.employeeRepository.save(user)
  }

  async deleteEmployeeById(id: number) {
    try {
      return await this.employeeRepository.delete(id)
    } catch (error) {
      
    }
  }

}
