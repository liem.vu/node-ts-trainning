import { Inject, Service } from 'typedi';
import admin from 'firebase-admin';
import { IFirebaseUser, IFirebaseUserUpdate } from '../interfaces/IFirebaseAuth';
import CustomError from '../utils/customError';

@Service()
export default class FirebaseAuthService {
  constructor(@Inject('logger') private logger, @Inject('firebaseAdmin') private firebaseAdmin: typeof admin) {}

  public async signIn(uid): Promise<string> {
    return this.firebaseAdmin
      .auth().createCustomToken(uid, { name: 'Liem dep trai'}).then(userRecord => {
        return userRecord;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.getUser] Get user failed: ' + error);
        return null;
      });
  }

  public async getUser(id): Promise<string> {
    return this.firebaseAdmin
      .auth()
      .getUser(id)
      .then(userRecord => {
        return userRecord.uid;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.getUser] Get user failed: ' + error);
        return null;
      });
  }

  public async getUserByPhone(phone: string): Promise<string> {
    return this.firebaseAdmin
      .auth()
      .getUserByPhoneNumber(phone)
      .then(userRecord => {
        return userRecord.uid;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.getUserByPhone] Get user failed: ' + error);
        return null;
      });
  }

  private async removeUserByPhone(phone: string, uid?: string): Promise<void> {
    const regexp: RegExp = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;
    return this.firebaseAdmin
      .auth()
      .getUserByPhoneNumber(phone)
      .then(userRecord => {
        console.log(userRecord.uid, uid);
        console.log(regexp.test(userRecord.uid));
        
        if (uid && userRecord.uid !== uid && !regexp.test(userRecord.uid)) {
          return this.firebaseAdmin.auth().deleteUser(userRecord.uid);
        }
        return {};
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.removeUserByPhone] Remove user failed: ' + error);
        return null;
      });
  }

  public async createUser(params: IFirebaseUser): Promise<string> {
    await this.removeUserByPhone(params.phoneNumber);
    
    return this.firebaseAdmin
      .auth()
      .createUser(params)
      .then(userRecord => {
        console.log('userRecord', userRecord);
        return userRecord.uid;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.createUser] Create user failed: ' + error);
        return null;
      });
  }

  public async updateUser(userId, params: IFirebaseUserUpdate): Promise<string> {
    await this.removeUserByPhone(params.phoneNumber, userId);
    return this.firebaseAdmin
      .auth()
      .updateUser(userId, params)
      .then(userRecord => {
        console.log('update succs');
        
        return userRecord.uid;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.updateUser] Update user failed: ' + error);
        return null;
      });
  }

  public async deleteUser(userId): Promise<void> {
    return this.firebaseAdmin
      .auth()
      .deleteUser(userId)
      .then(() => {
        return;
      })
      .catch(error => {
        this.logger.error('[FirebaseAuthService.deleteUser] Delete user failed: ' + error);
        return;
      });
  }

  public async decodeIdToken(idToken: string): Promise<any> {
    return this.firebaseAdmin
      .auth()
      .verifyIdToken(idToken)
      .then(decodeToken => {
        return decodeToken;
      })
      .catch(e => {
        console.log(e);
        
        if (e instanceof CustomError) {
          throw e;
        }
        throw new CustomError(500, 'FIREBASE_TOKEN_INVALID', 'FIREBASE_TOKEN_INVALID');
      });
  }
}
