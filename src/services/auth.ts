import { Inject, Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import _ from 'lodash';

import appConstant from '../utils/appConstant';
import { comparePassword } from '../utils/commonUtils';
import CustomError from '../utils/customError';

import config from '../config';

import { UserRepository } from '../repositories/user';

import axios from 'axios';
import { IUser } from '../interfaces/IUser';
import { IClient, IOAuth2Client } from '../interfaces/IClient';
import { IAccessToken, IOAuth2RefreshToken, IOAuth2Token, IToken } from '../interfaces/IToken';
import { ClientRepository } from '../repositories/client';
import { TokenRepository } from '../repositories/token';
import FirebaseAuthService from './firebaseAuth';

/**
 * @class AuthService
 */
@Service()
export default class AuthService {
  constructor(
    @Inject('logger') private logger,
    @InjectRepository() private readonly userRepository: UserRepository,
    @InjectRepository() private readonly clientRepository: ClientRepository,
    @InjectRepository() private readonly tokenRepository: TokenRepository,
    private firebaseAuthService: FirebaseAuthService
  ) {}

  private async generateToken(client: IOAuth2Client, user: IUser, scope: string, lifetime: number): Promise<string> {
    const now = moment().unix();
    const payload = {
      iat: now,
      exp: now + lifetime,
      uid: user.id,
      claims: {
        user_id: user.id,
        username: user.username,
        client_id: client.id,
      },
      scope: scope,
    };

    return jwt.sign(payload, config.jwt.key);
  }

  private verifyToken = (token: string) => {
    const decodedToken = jwt.decode(token);

    if (!decodedToken || decodedToken.exp < moment().unix()) {
      return null;
    }

    return decodedToken;
  };

  public async generateAccessToken(client: IOAuth2Client, user: IUser, scope: string): Promise<string> {
    console.log('4.Token|generateAccessToken', user);
    return this.generateToken(client, user, scope || '', appConstant.ACCESS_TOKEN_LIFETIME);
  }

  public async generateRefreshToken(client: IOAuth2Client, user: IUser, scope: string): Promise<string> {
    console.log('5.generateRefreshToken');
    return this.generateToken(client, user, scope || '', appConstant.REFRESH_TOKEN_LIFETIME);
  }

  //region Used by auth2-server
  public async getAccessToken(
    accessToken: string,
  ): Promise<{
    accessToken: string;
    accessTokenExpiresAt: Date;
    client: { id: string };
    user: IUser;
    scope: string;
  } | void> {
    console.log('1.Authenticate|getAccessToken', accessToken);
    const decodeToken = this.verifyToken(accessToken);
    
    if (!decodeToken) {
      return;
    }
    const tokens = await this.tokenRepository.find({ user_id: decodeToken.uid });
    const token = _.find(tokens, { access_token: accessToken });
    if (!token) {
      return;
    }

    const user = await this.userRepository.findOne({id:token.user_id})

    return {
      accessToken: token.access_token,
      accessTokenExpiresAt: token.access_token_expires_at,
      client: { id: token.client_id },
      user,
      scope: decodeToken.scope || '',
    };
  }

  public async getRefreshToken(
    refreshToken: string,
  ): Promise<{
    accessToken: string;
    accessTokenExpiresAt: Date;
    refreshToken: string;
    refreshTokenExpiresAt: Date;
    client: { id: string };
    user: { id: string; roles: string[]; slug: string };
    scope: string;
  } | void> {
    console.log('Liem 225');
  }

  public async getClient(
    clientId: string,
    clientSecret: string,
  ): Promise<{
    id: string;
    clientSecret: string;
    grants: string[];
  } | void> {
    console.log('1.Token|getClient');
    const client = await this.clientRepository.findOne({
      client_id: clientId,
      client_secret: clientSecret,
    });
    if (!client) {
      return null;
    }
    console.log('client', client);
    
    return {
      id: client.client_id,
      clientSecret: client.client_secret,
      grants: ['password', 'social_network', 'firebase_auth', 'refresh_token'],
    };
  }

  public async getUser(username: string, password: string): Promise<IUser | void> {
    console.log('2.Token|getUser');
    const user = await this.userRepository.findOne({username});
    if (!user) {
      return;
    }

    const isPasswordCorrect = await comparePassword(password, user.password);
    if (!isPasswordCorrect) {
      console.log('incorect');
      return;
    }
    console.log('corect');

    return user
  }

  public async saveToken(token: IOAuth2Token, client: IOAuth2Client, user: IUser): Promise<IAccessToken> {
    console.log('6.Token|saveToken');
    const _token = {} as IToken;
    _token.user_id = user.id;
    _token.client_id = client.id;
    _token.access_token = token.accessToken;
    _token.access_token_expires_at = token.accessTokenExpiresAt;
    _token.refresh_token = token.refreshToken;
    _token.refresh_token_expires_at = token.refreshTokenExpiresAt;

    // As user can only log in on one device at the same time, we need to remove all existed token before saving a new one
    await this.tokenRepository.delete({ user_id: user.id });
    await this.tokenRepository.save(_token);
    return {
      client: { id: _token.client_id },
      user,
      accessToken: _token.access_token,
      accessTokenExpiresAt: _token.access_token_expires_at,
      refreshToken: _token.refresh_token,
      refreshTokenExpiresAt: _token.refresh_token_expires_at,
    };
  }

  public async revokeToken(token: IOAuth2RefreshToken): Promise<any> {
    console.log('revokeToken');
  }

  public validateScope(user: IUser, client: IClient, scope: string) {
    console.log('3.Token|validateScope');
    
    const validScope = appConstant.SCOPE_SERVICES;

    return scope && (!scope.split(' ').every(s => validScope.indexOf(s) >= 0) ? false : scope);
  }

  public async verifyScope(token: {
    accessToken: string;
    accessTokenExpiresAt: Date;
    client: { id: string };
    user: { id: string };
    scope: string;
  }) {
    console.log('2.Authenticate|verifyScope.');
    const requestedScope = token.scope.split(' ');
    const client = await this.clientRepository.findOne({ client_id: token.client.id });
    const authorizeScope = client.scope.split(' ');
    console.log(requestedScope.every(s => authorizeScope.indexOf(s) >= 0));
    

    return requestedScope.every(s => authorizeScope.indexOf(s) >= 0);
  }
  //endregion

  /**
   * Used by {@link FirebaseTokenGrantType}
   * @param {string} firebase_token
   * @returns {Promise<{id: string, username: string} | void>}
   */
   public async getUserByFirebaseToken(firebase_token: string): Promise<IUser | void> {
    console.log('Firebase|getUserByFirebaseToken', firebase_token);
     
    const { uid } = await this.firebaseAuthService.decodeIdToken(firebase_token);
    // const { uid } = { uid: 'f8dddfed-a8a1-419e-b60c-8e499a33d003' };
    console.log('uid', uid);
    
    const user = await this.userRepository.findOne({id: uid});
    if (!user) {
      return;
    }

    return user
  }
}
