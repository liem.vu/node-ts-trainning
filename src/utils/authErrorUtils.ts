import OAuthError from 'oauth2-server/lib/errors/oauth-error';
import logger from '../loaders/logger';

/**
 * Convert error of oauth to meaningful error
 * @param {OAuthError} errorObj
 * @param {string} lang
 * @returns {IAuthError}
 */
export function convertOAuthError(errorObj: OAuthError, lang?: string) {
  const { code, name, message } = errorObj;
  let convertedName;
  let convertedMsg;

  switch (name) {
    case 'access_denied':
      convertedName = 'ACCESS_DENIED';
      break;
    case 'insufficient_scope':
    case 'invalid_argument':
    case 'invalid_client':
    case 'invalid_request':
    case 'invalid_scope':
    case 'unauthorized_client':
    case 'unauthorized_request':
    case 'unsupported_grant_type':
    case 'unsupported_response_type':
      convertedName = 'INVALID_LOGIN_INFO';
      break;
    case 'invalid_grant':
      if (message === 'Invalid grant: user credentials are invalid') {
        convertedName = 'INCORRECT_USERNAME_PASSWORD';
      } else if (message === 'Invalid grant: refresh token is invalid') {
        convertedName = 'INVALID_SESSION';
      } else if (message === 'Invalid grant: refresh token has expired') {
        convertedName = 'SESSION_EXPIRED';
      }
      break;
    case 'invalid_token':
      if (message === 'Invalid token: access token is invalid') {
        convertedName = 'INVALID_SESSION';
      } else {
        convertedName = 'SESSION_EXPIRED';
      }
      break;
    case 'server_error':
      if (message === 'ACCOUNT_BLOCKED') {
        convertedName = 'BLOCKED';
      } else if (message === 'FIREBASE_TOKEN_INVALID') {
        convertedName = 'FIREBASE_TOKEN_INVALID';
      } else {
        convertedName = 'OAUTHERROR';
        convertedMsg = 'Internal Server Error';
      }
      break;
    default:
      convertedName = 'OAUTHERROR';
      convertedMsg = 'Internal Server Error';
  }

  return {
    code,
    name: convertedName,
    message: convertedMsg,
  };
}
