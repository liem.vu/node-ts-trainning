import config from '../config';

const SERVICES_ENDPOINT = {};

export default {
  authenticate: config.endpoint.auth_service + '/api/v1/users/authenticate',
};
