export default class AppConstant {
  // public static ACCESS_TOKEN_LIFETIME = 60 * 60 * 12;
  public static ACCESS_TOKEN_LIFETIME = 60 * 60 * 0.5;
  // public static REFRESH_TOKEN_LIFETIME = 60 * 60 * 24 * 30;
  public static REFRESH_TOKEN_LIFETIME = 60 * 60 * 24 * 1;

  public static SCOPE_SERVICES = ['UserService', 'LiftService', 'MediaService'];
  
  public static SUPPORT_LANGUAGE = ['vi', 'en'];
  public static DEFAULT_LANGUAGE = 'en';
  public static API_REF = {
    PRE_FIX: '/user-service',
    OPEN_API: '/oapi',
    INTERNAL_API: '/internal',
    EXTERNAL_API: '/external',
    AUTH_API: '/api',
    VERSION: '/v1',
  };

  public static DEFAULT_PAGINATE = {
    MAX: 10,
    SKIP: 1,
  };

  public static PAGING_PROP = {
    limit: 'take',
    page: 'skip',
  };

}
