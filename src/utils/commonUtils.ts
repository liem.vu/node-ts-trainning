import _ from 'lodash';
import { IListPaginate, IPaginated } from '../interfaces/ICommonDef';
import bcrypt from 'bcrypt';

export async function hashPassword(plainText: string): Promise<string> {
  return await bcrypt.hash(plainText, 8);
}

export function getPhoneNumberWithDialCode(phone: string, dialCode: string) {
  return phone.replace('0', dialCode);
}

export async function comparePassword(password: string, storedHash: string): Promise<boolean> {
  return await bcrypt.compare(password, storedHash);
}

export function parseJson(input) {
  try {
    return JSON.parse(input);
  } catch (e) {
    return null;
  }
}

export function slugify(text) {
  if (!text) {
    return text;
  }

  return (
    text
      .toString()
      .trim()
      .toLowerCase()
      // Transform diacritics to non-diacritics
      .replace(/[áàảạãăắằẳẵặâấầẩẫậ]/gi, 'a')
      .replace(/[éèẻẽẹêếềểễệ]/gi, 'e')
      .replace(/[iíìỉĩị]/gi, 'i')
      .replace(/[óòỏõọôốồổỗộơớờởỡợ]/gi, 'o')
      .replace(/[úùủũụưứừửữự]/gi, 'u')
      .replace(/[ýỳỷỹỵ]/gi, 'y')
      .replace(/đ/gi, 'd')
      // Remove special char
      .replace(/[`~!@#|$%^&*()+=,.\\/?><'“:;_]/gi, '')
      // Transform space char to hyphen
      .replace(/\s/gi, '-')
      // Transform continuous hyphen to hyphen
      .replace(/-+/gi, '-')
      // Remove start/end hyphen
      .replace(/^-+|-+$/g, '')
  );
}

export function transformData(plain: any[]): any[] {
  const clone = Array.isArray(plain) ? plain : [plain];
  for (const _p of clone) {
    for (const [key, val] of Object.entries(_p)) {
      _p[key] = parseJson(val) || val;
    }
  }

  return plain;
}
export async function generateDbSlug(evalSlug: string, alikeSlugs: string[]): Promise<string> {
  let nameSlug = slugify(evalSlug);
  if (!_.isEmpty(alikeSlugs)) {
    const listSuffixSlugId = alikeSlugs
      // Get suffix of like-name-slug
      .map(i => Number(i.replace(nameSlug, '')))
      // Remove non-digits
      .filter(i => !isNaN(i));

    if (!_.isEmpty(listSuffixSlugId)) {
      nameSlug = nameSlug + (Math.max(...listSuffixSlugId) + 1);
    }
  }

  return nameSlug;
}

export function getItemAndIndex(collection: any[], field: string, value: any): [number, any] {
  for (const item of Object.entries(collection)) {
    if (item) {
      if (item[1][field] === value) {
        return [Number(item[0]), item[1]];
      }
    }
  }

  return [0, null];
}

export function getEnumValues(input: any) {
  return Object.values(input).filter(x => !isNaN(Number(x)));
}

/**
 * Replace string in template. This will find the anchors defined in template then replace with provided value
 * An anchor should be followed format: %anchor_name%
 * 'replacer' is an object with key is anchor name and value of key is the value of the anchor
 *
 * @example
 *    {
 *      const template = 'Hello %someone%'
 *      const replacer = {
 *        someone: 'World'
 *      }
 *    }
 *
 * An alternative way to use this function is provide an Array value that will be used to replace for 'replacer' param
 * In this case, template anchor should be a number index
 *
 * @example
 *    {
 *      const template = 'Hello %0%, I'm your %1%'
 *      const replacer = ['world', 'Lord']
 *    }
 *
 *
 * @param {String} template
 * @param {Object} replacer
 * @return {string|Object|*|void}
 */
export function stringReplacer(template, replacer) {
  return template.replace(/%([\w]+)?%/g, ($1, $2) => replacer[$2]);
}

export function wrapPagination(data: any[], totalCount: number, paginationCfg: IPaginated): IListPaginate<any> {
  return {
    data: data,
    total_pages: Math.ceil(totalCount / paginationCfg.limit),
    limit: paginationCfg.limit,
    page: paginationCfg.page,
    total_records: totalCount,
  };
}

export function extractSorting(string): { key: string; dir: 'ASC' | 'DESC' } {
  const [sortKey, sortDir] = string.split(' ');

  return {
    key: sortKey,
    dir: sortDir.toUpperCase(),
  };
}
