import { IOrder, IPaginated } from '../interfaces/ICommonDef';
import { FindManyOptions, SelectQueryBuilder } from 'typeorm';
import appConstant from './appConstant';

/**
 * Split off given condition into Dto and paging config
 * @param {*} queryObj
 * @param {FindManyOptions} condition
 * @returns {FindManyOptions}
 */
export function buildPaging<Entity>(queryObj: any, condition: FindManyOptions<Entity>): FindManyOptions<Entity> {
  // Build `paging` query
  for (const [prop, val] of Object.entries(appConstant.PAGING_PROP)) {
    if (Object.prototype.hasOwnProperty.call(queryObj, prop)) {
      condition[val] = prop === 'page' ? queryObj['limit'] * (queryObj[prop] - 1) : queryObj[prop];
    }
  }

  // Build `order` query
  condition.order = _serializeBuilder(queryObj as IPaginated) as FindManyOptions<Entity>['order'];

  return condition;
}

// TODO refactor typeorm issue paginate use take skip
export function buildQueryBuilderPaging(
  queryInfo: IPaginated,
  queryObj: SelectQueryBuilder<any>,
): SelectQueryBuilder<any> {
  queryObj.take(queryInfo.limit).skip(queryInfo.limit * (queryInfo.page - 1));

  return queryObj;
}

// TODO refactor typeorm issue paginate with raw query use limit and offset
export function buildRawQueryPaging(queryInfo: IPaginated, queryObj: SelectQueryBuilder<any>): SelectQueryBuilder<any> {
  queryObj.limit(queryInfo.limit).offset(queryInfo.limit * (queryInfo.page - 1));

  return queryObj;
}

export function buildQueryBuilderSorting(
  sortingInfo: { key: string; dir: 'ASC' | 'DESC' },
  queryObj: SelectQueryBuilder<any>,
  alias?: string,
): SelectQueryBuilder<any> {
  const { key: sortKey, dir: sortDir } = sortingInfo;
  queryObj.orderBy(alias ? alias + '.' + sortKey : sortKey, sortDir);

  return queryObj;
}

/**
 * @example
 * const raw = {
 *   user_id: 1,
 *   user_full_name: "Tan",
 *   user_status: 1,
 *   profile_company_name: "Fractal",
 *   profile_gender: "mr"
 * }
 *
 * plainToEntities(raw, ['user', 'profile'])
 *
 * result = {
 *   user: {
 *     id: 1,
 *     full_name: "Tan",
 *     status: 1
 *   },
 *   profile: {
 *     company_name: "Fractal",
 *     gender: "mr"
 *   }
 * }
 * @param raw
 * @param {string[]|string} keyGroups
 * @param mainKey
 * @returns {any}
 */
export function plainRawToEntities(raw: any, keyGroups: string[] | string, mainKey?: string): any {
  const isArrayInput = Array.isArray(raw);
  const keyGroupArr = Array.isArray(keyGroups) ? keyGroups : [keyGroups];
  const arr = isArrayInput ? raw : [raw];
  const results = [];
  const keyInit = (result: any, keyGroups: string[]): any => {
    for (const keyGroup of keyGroups) {
      result[keyGroup] = {};
    }
  };
  const keyInjector = _generateKeyInjector(keyGroupArr);

  for (const item of arr) {
    keyInit(item, keyGroupArr);
    for (const keyGroup of keyGroupArr) {
      keyInjector(item, keyGroup);
    }

    if (mainKey) {
      const pattern = mainKey + '_';
      for (const [_k, _v] of Object.entries(item)) {
        if (_k.indexOf(pattern) === 0) {
          const trim = _k.replace(pattern, '');
          item[trim] = _v;
          delete item[_k];
        }
      }
    }

    results.push(item);
  }

  return isArrayInput ? results : results[0];
}

/**
 * @param {string[] | string} keyGroup
 * @returns {Function}
 */
function _generateKeyInjector(keyGroup: string[] | string): any {
  const keyGroupArr = Array.isArray(keyGroup) ? keyGroup : [keyGroup];
  let initStr = '';

  initStr += 'return (source) => { ';
  initStr += 'for (const [_key, _val] of Object.entries(source)) { ';
  for (const [index, value] of Object.entries(keyGroupArr)) {
    initStr += `const regex${index} = /^${value}_/; `;
    initStr += `if (regex${index}.test(_key)) { `;
    initStr += `source['${value}'][_key.replace(regex${index}, '')] = _val; `;
    initStr += 'delete source[_key]; ';
    initStr += 'continue;} ';
  }
  initStr += '}} ';
  return new Function(initStr)();
}

/**
 * `order` in paging is pass by following format: [<field>-<dir>, <field>-<dir>,....]
 * It should be converted to { field: dir }
 * @param {IPaginated} pagingObj
 * @returns {IOrder}
 * @private
 */
function _serializeBuilder(pagingObj: IPaginated): IOrder {
  const formattedOrderObj: any = {};
  if (pagingObj.sorting) {
    const orderCfg: string = pagingObj.sorting;
    const orderReg = /(?<column>[a-z]+(_[a-z]+)?)\s(?<dir>(asc|desc))$/;
    const result = orderReg.exec(orderCfg);

    if (result) {
      const { column, dir } = result.groups as { column: string; dir: string };
      if (column && dir) {
        formattedOrderObj[column] = dir.toUpperCase();
      }
    }
  }

  return formattedOrderObj;
}

export function buildTimePeriodCondition<T>(
  query: SelectQueryBuilder<T>,
  params: any,
  config: { alias?: string; column: string; fromKey: string; toKey: string },
): void {
  const from = params[config.fromKey];
  const to = params[config.toKey];
  const alias = config.alias ? config.alias + '.' : '';
  if (from && to) {
    query.andWhere(`${alias}${config.column} BETWEEN :from AND :to`, {
      from,
      to,
    });
  } else if (from) {
    query.andWhere(`${alias}${config.column} >= :from`, { from });
  } else if (to) {
    query.andWhere(`${alias}${config.column} <= :to`, { to });
  }
}
