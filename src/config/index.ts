import dotenv from 'dotenv';
import path from 'path';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const envPath = process.env.NODE_ENV === 'development' ? '.env' : `.env.${process.env.NODE_ENV}`;

const envFound = dotenv.config({ path: path.join(__dirname, `../../${envPath}`) });
if (envFound.error) {
  // This error should crash whole process

  // eslint-disable-next-line prettier/prettier
  throw new Error('Couldn\'t find .env file');
}

export default {
  /**
   * Your favorite port
   */
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 8080,

  /**
   * Database config
   */

  database: {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT ? parseInt(process.env.DATABASE_PORT) : 3306,
    user: process.env.DATABASE_USER,
    pass: process.env.DATABASE_PASS,
    dbName: process.env.DATABASE_NAME,
    synchronize: process.env.DATABASE_SYNC ? JSON.parse(process.env.DATABASE_SYNC) || false : false,
  },


  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },
  socket: {
    nsp_notification: process.env.SOCKET_NSP_NOTIFY,
    event: {
      notification: 'message:notification',
    },
  },

  jwt: {
    key: process.env.JWT_KEY,
  },
  /**
   * Service configuration
   */
  api_key: process.env['APIKEY'] || 'abc:xyz',
  endpoint: {
    internal_gateway: process.env['INTERNAL_GATEWAY'],
    auth_service: process.env['AUTH_SERVICE'] + '/user-service',
  },
};
