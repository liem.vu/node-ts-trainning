import path from 'path';
import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

const transports = [];
if (process.env.NODE_ENV !== 'development') {
  transports.push(new winston.transports.Console());
} else {
  transports.push(
    new winston.transports.Console({
      format: winston.format.combine(winston.format.cli(), winston.format.splat()),
    }),
  );
}

const options = {
  infoFile: {
    level: 'info',
    name: 'file.info',
    // eslint-disable-next-line no-undef
    filename: path.resolve(__dirname, '../', 'logs', '%DATE%.log'),
    handleExceptions: true,
    json: true,
    colorize: true,
    datePattern: 'YYYY-MM-DD-HH',
    // maxSize: '50m',
    maxsize: 5242880,
    maxFiles: '14d',
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

winston.transports.DailyRotateFile = DailyRotateFile;
const LoggerInstance = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf(info => {
      return `[${info.timestamp}] [${info.level}]: ${
        typeof info.message === 'object' ? JSON.stringify(info.message) : info.message
      }`;
    }),
  ),
  transports: [
    new winston.transports.Console(options.console),
    new winston.transports.DailyRotateFile(options.infoFile),
  ],
  exitOnError: false, // do not exit on handled exceptions
});

export default LoggerInstance;
