import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import path from 'path';
import i18n from 'i18n';

import routes from '../api/routes';
import errorHandler from '../api/middlewares/errorHandlerMiddleware';
import appConstant from '../utils/appConstant';

export default ({ app }: { app: express.Application }) => {

  // Useful if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
  // It shows the real origin IP in the heroku or Cloudwatch logs
  app.enable('trust proxy');

  // The magic package that prevents frontend developers going nuts
  // Alternate description:
  // Enable Cross Origin Resource Sharing to all origins by default
  app.use(cors());

  // Middleware that transforms the raw string of req.body into json
  app.use(express.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  i18n.configure({
    locales: ['en', 'vi'],
    directory: path.join(__dirname, '../config/locales'),
    objectNotation: true,
  });


  // Load API routes
  app.use(appConstant.API_REF.PRE_FIX, routes());

  /// error handlers
  app.use(errorHandler);
};
