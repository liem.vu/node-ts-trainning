import { createConnection, Connection, useContainer } from 'typeorm';
import { Container } from 'typedi';
import path from 'path';
import config from '../config';

export default async (): Promise<Connection> => {
  // TypeOrm + TypeDI
  useContainer(Container);
  const connection = await createConnection({
    type: 'mysql',
    host: config.database.host,
    port: config.database.port,
    username: config.database.user,
    password: config.database.pass,
    database: config.database.dbName,
    entities: [path.join(__dirname, '../entity/*.{js,ts}')],
    synchronize: config.database.synchronize,
    bigNumberStrings: false,
    timezone: 'Z',
  });

  return connection;
};
