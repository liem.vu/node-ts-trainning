import { Container } from 'typedi';
import LoggerInstance from './logger';
import FirebaseAdmin from './firebase';

export default () => {
  try {
    // DI logger
    Container.set('logger', LoggerInstance);
    Container.set('firebaseAdmin', FirebaseAdmin);
  } catch (e) {
    LoggerInstance.error('🔥 Error on dependency injector loader: %o', e);
    throw e;
  }
};
