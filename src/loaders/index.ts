import typeOrmLoader from './typeorm';
import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import socketIOLoader from './socket';
import eventLoader from './events';
import Logger from './logger';
import express from 'express';
import { Container } from 'typedi';
import FirebaseAuthService from '../services/firebaseAuth';
// import eventLoader from './events';

export default async ({ expressApp, httpServer }: { expressApp: express.Express, httpServer?:any }) => {
  // await typeOrmLoader();
  Logger.info('✌️ DB loaded and connected!');

  await dependencyInjectorLoader();

  Logger.info('✌️ Dependency Injector loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');

  await eventLoader();
  Logger.info('✌️ All event loaded');

  const ioNsps = await socketIOLoader({ httpServer });
  console.log(ioNsps);
  
  ioNsps.forEach(v => {
    Container.set(v.name, v.ioNsp);
  });
  Logger.info('✌️ Socket IO loaded');
};
