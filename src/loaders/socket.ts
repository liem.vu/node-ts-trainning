import { createProxyServer } from 'http-proxy';
import { Server } from 'socket.io';

import socketServer from '../socket/socketServer';

export default ({ httpServer }) => {
  // TODO config socket
  const socketio = new Server(httpServer, {
    cors: {
      origin: '*',
    },
  });

  const proxyServer = createProxyServer({ ws: true });
  socketio.on('upgrade', function (req, socket, head) {
    proxyServer.ws(req, socket, head);
  });
  const ioNsps = socketServer.start(socketio);

  return ioNsps;
};
