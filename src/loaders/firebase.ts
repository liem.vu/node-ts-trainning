import FirebaseAdmin from 'firebase-admin';
import path from 'path';

const config = path.join(__dirname, '../config/firebase.config.json');

FirebaseAdmin.initializeApp({
  credential: FirebaseAdmin.credential.cert(config),
});

export default FirebaseAdmin;
