import { CronJob } from 'cron';

export default class EmployeeJobs {
  public async testJob() {
    const cronTime = '*/60 * * * * *';

    return new CronJob(
      cronTime,
      function () {
        console.log('Test CronJob')
      },
      null,
      true,
      'GMT',
    );
  }
}
