import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ContacInfo } from './ContacInfo';
import { Employee } from './Employee';


@Entity()
export class Task {
  constructor(config?: { [P in keyof Task]?: Task[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(()=> Employee, employee => employee.tasks, { onDelete: 'SET NULL' })
  employee: Employee;
}