import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Employee } from './Employee';


@Entity()
export class Meeting {
  constructor(config?: { [P in keyof Meeting]?: Meeting[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  zoomUrl: string;

  @ManyToMany(()=> Employee, employee => employee.meetings)
  attendees: Employee[];
}