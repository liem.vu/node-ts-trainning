import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Client {
  constructor(config?: { [P in keyof Client]?: Client[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn('uuid')
  client_id: string;

  @Column({ nullable: false })
  client_secret: string;

  @Column()
  redirect_uri: string;

  @Column({ nullable: false })
  scope: string;
}
