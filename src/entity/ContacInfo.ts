import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Employee } from "./Employee";


@Entity()
export class ContacInfo {
    constructor(config?: { [P in keyof ContacInfo]?: ContacInfo[P] }) {
        if (config) {
          Object.assign(this, { ...config });
        }
      }
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    phone: string;

    @Column()
    email: string;

    @Column()
    employeeId: number;

    @OneToOne(() => Employee, employee => employee.contactInfo, { onDelete: 'CASCADE' })
    @JoinColumn()
    employee: Employee
}