import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Token {
  constructor(config?: { [P in keyof Token]?: Token[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  user_id: string;

  @Column({ nullable: false })
  client_id: string;

  @Column('varchar', { length: 1000, nullable: false })
  access_token: string;

  @Column({ type: 'timestamp', nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  access_token_expires_at: Date;

  @Column('varchar', { length: 1000, nullable: false })
  refresh_token: string;

  @Column({ type: 'timestamp', nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  refresh_token_expires_at: Date;
}
