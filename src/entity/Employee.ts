import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ContacInfo } from './ContacInfo';
import { Meeting } from './Meeting';
import { Task } from './Task';


@Entity()
export class Employee {
  constructor(config?: { [P in keyof Employee]?: Employee[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Employee, employee => employee.directReports, { onDelete: 'SET NULL' })
  manager: Employee;

  @OneToMany(()=> Employee, employee => employee.manager)
  directReports: Employee[]

  @OneToOne(()=> ContacInfo, contactInfo => contactInfo.employee)
  contactInfo: ContacInfo;

  @OneToMany(()=> Task, task => task.employee)
  tasks: Task[]

  @ManyToMany(()=> Meeting, meeting => meeting.attendees)
  @JoinTable()
  meetings: Meeting[]
}