import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';


@Entity()
export class User {
  constructor(config?: { [P in keyof User]?: User[P] }) {
    if (config) {
      Object.assign(this, { ...config });
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  fullname: string;

  @Column()
  phone: string;

  @Column()
  dial_code: string;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column()
  password: string;
  
  @Column()
  fcm_token: string;
}