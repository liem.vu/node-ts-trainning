import 'reflect-metadata'; // We need this in order to use @Decorators
import express from 'express';
import Loaders from './loaders';
import swaggerJsdoc from 'swagger-jsdoc';
import AppConstant from './utils/appConstant';
import { createServer } from 'http';

async function startServer() {

  const options = {
    definition: {
      openapi: '3.0.0',
      servers: [
        {
          description: 'Lift Service Dev',
          url: 'https://test-public-api-gsec.fractal.vn/lift-service',
        },
        // {
        //   url: `http://localhost:${config.port}/lift-service`,
        // },
      ],
      info: {
        description: 'API endpoint specification for GS E&C',
        version: '1.0.0',
        title: 'GS E&C API',
        license: {
          name: 'Apache 2.0',
          url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
        },
      },
      components: {
        securitySchemes: {
          BearerAuth: {
            type: 'http',
            scheme: 'bearer',
          },
        },
      },
      security: [
        {
          BearerAuth: [],
        },
      ],
      tags: [
        {
          name: 'Task',
          description: 'Task Management',
        },
      ],
    },
    apis: ['./src/**/*.ts'],
  };

  const openapiSpecs = await swaggerJsdoc(options);

  const app = express();
  const httpServer = createServer(app);

  /**
   * A little hack here
   * Import/Export can only be used in 'top-level code'
   * Well, at least in node 10 without babel and at the time of writing
   * So we are using good old require.
   */
   await Loaders({ expressApp: app, httpServer });

   await httpServer.listen(3000);
   console.log('Server listening on port 3000')
   
   app.get(AppConstant.API_REF.PRE_FIX + '/api-docs', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(openapiSpecs);
  });

  return app;
}

startServer();
