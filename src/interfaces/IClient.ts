import { Client } from "../entity/Client";

export type IClient = Client;

export interface IOAuth2Client {
  id: string;
  clientSecret: string;
  redirectUri: string;
  scope: string;
}
