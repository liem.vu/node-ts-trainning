export interface IFirebaseUser {
  uid: string;
  email: string;
  phoneNumber: string;
  disabled?: boolean;
}

export interface IFirebaseUserUpdate {
  email?: string;
  phoneNumber?: string;
  disabled?: boolean;
}
