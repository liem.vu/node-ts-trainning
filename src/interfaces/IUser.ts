import { User } from "../entity/User";
import { IPaginated } from "./ICommonDef";


export type IUser = User;

export interface IUserQuery extends IPaginated {
    filter?: string;
}