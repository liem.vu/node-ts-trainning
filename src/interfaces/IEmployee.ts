import { Employee } from "../entity/Employee";
import { IPaginated } from "./ICommonDef";


export type IEmployee = Employee;

export interface IEmployeeQuery extends IPaginated {
    filter?: string;
}