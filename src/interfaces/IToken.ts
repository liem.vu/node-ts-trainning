import { Token } from '../entity/Token';
import { IOAuth2Client } from './IClient';
import { IUser } from './IUser';

export type IToken = Token;

export interface IOAuth2Token {
  accessToken: string;
  accessTokenExpiresAt: Date;
  refreshToken: string;
  refreshTokenExpiresAt: Date;
  scope?: string[];
}

export interface IOAuth2RefreshToken {
  refreshToken: string;
  refreshTokenExpiresAt: Date;
}

/**
 * @openapi
 *  components:
 *    schemas:
 *      AccessTokenSchema:
 *        type: object
 *        properties:
 *          client:
 *            type: object
 *            description: Client endpoint information.
 *            properties:
 *              id:
 *                type: string
 *                description: Client ID. This is the one you passed when performing logging
 *                example: 'fractal_id'
 *          user:
 *            description: User info
 *            type: object
 *            properties:
 *              id:
 *                type: string
 *                description: User Id
 *                example: '3f9afea4-...-8ccdbeec524d'
 *              slug:
 *                type: string
 *                description: User slug generated automatically from server based on User's full name
 *                example: 'quach-tinh-lang3'
 *              roles:
 *                description: Role of user
 *                type: array
 *                items:
 *                  type: string
 *                  example: 'engineer'
 *          accessToken:
 *            type: string
 *            description: Token used to access the API
 *            example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...'
 *          accessTokenExpiresAt:
 *            description: The time token is expired
 *            type: string
 *            format: date-time
 *            example: '2021-04-27T14:09:27.000Z'
 *          refreshToken:
 *            type: string
 *            description: Token used to refresh access token
 *            example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...'
 *          refreshTokenExpiresAt:
 *            description: The time refresh token is expired
 *            type: string
 *            format: date-time
 *            example: '2021-04-27T14:09:27.000Z'
 */
export interface IAccessToken {
  client: Pick<IOAuth2Client, 'id'>;
  user: IUser;
  accessToken: string;
  accessTokenExpiresAt: Date;
  refreshToken: string;
  refreshTokenExpiresAt: Date;
}
