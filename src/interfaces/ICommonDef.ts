/**
 * @openapi
 *  components:
 *    schemas:
 *      Error:
 *        type: object
 *        properties:
 *          errorCode:
 *            type: string
 *          message:
 *            type: string
 *        description: Object error code
 */

/**
 * @openapi
 *  components:
 *    examples:
 *      InternalError:
 *        value:
 *          errorCode: UNHANDLED_ERROR
 *          message: Server internal error!
 */

/**
 * @openapi
 *  components:
 *    examples:
 *      dataInvalidExample:
 *        value:
 *          errorCode: 'DATA_INVALID'
 *          message: 'Field_name invalid'
 *      alarmIdExistedExample:
 *        value:
 *          errorCode: 'ALARM_ID_EXISTED'
 *          message: 'Alarm ID already exists'
 *      notFoundExample:
 *        value:
 *          errorCode: 'NOT_FOUND'
 *          message: 'Lift not found!'
 *      notAcceptedExample:
 *        value:
 *          errorCode: 'NOT_ACCEPTED'
 *          message: 'This lift is no available to book a task!'
 */

/**
 * @openapi
 *  components:
 *    examples:
 *      permissionDeniedExample:
 *        value:
 *          errorCode: 'PERMISSION_DENIED'
 *          message: 'Permission denied!'
 *      forbiddenExample:
 *        value:
 *          errorCode: 'FORBIDDEN'
 *          message: 'Forbidden'
 *      invalidTokenExample:
 *        value:
 *          errorCode: 'INVALID_TOKEN'
 *          message: 'Invalid token'
 */
export interface IPaginated {
  limit?: number;
  page?: number;
  sorting?: string;
}

export interface IOrder {
  [key: string]: 'ASC' | 'DESC';
}

export interface IQuery {
  filter?: string;
}

export interface IListPaginate<T> {
  total_records?: number;
  limit?: number;
  page?: number;
  total_pages?: number;
  data: T[];
}

export interface IFilterByDate {
  date_from?: Date;
  date_to?: Date;
}
