import axios from 'axios';
import logger from '../loaders/logger';
import endpointConstant from '../utils/endpointConstant';
import { IUser } from '../interfaces/IUser';
import CustomError from '../utils/customError';

class SocketValidator {
  public async validateAuth(accessToken): Promise<{ id: string } | IUser> {
    const matches = accessToken && accessToken.match(/Bearer\s(\S+)/);
    if (!matches) {
      throw new CustomError(401, 'INVALID_TOKEN', 'Invalid Token');
    }
    // Define header for services contact
    const headers = {};
    headers['Authorization'] = accessToken;
    const url = endpointConstant['authenticate'];
    console.log('url', url);
    
    try {
      const { data } = await axios.request({
        method: 'get',
        headers,
        url,
      });

      return data.user;
    } catch (e) {
      logger.error(`[SockerValidator.validataAuth] - Authentication access token err: ${e}`);
      throw new CustomError(401, 'INVALID_TOKEN', 'Invalid Token');
    }
  }
}

export default new SocketValidator();
