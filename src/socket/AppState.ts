import _ from 'lodash';

interface IUserActive {
  [key: string]: string[];
}

class AppState {
  private _users: IUserActive;
  constructor() {
    this._users = {};
  }

  public addSocket(socketId: string, userId: string) {
    if (socketId && userId) {
      if (!this._users[userId]) {
        this._users[userId] = [socketId];
      } else {
        if (!this._users[userId].includes[socketId]) {
          this._users[userId].push(socketId);
        }
      }
    }
  }

  public getSocketIds(userId: string) {
    if (userId) {
      return this._users[userId] || [];
    }
    return [];
  }

  public removeSocket(socketId, userId) {
    if (socketId && userId && this._users[userId]) {
      _.pull(this._users[userId], socketId);
      if (!this._users[userId].length) {
        delete this._users[userId];
      }
    }
  }

  public checkOnline(userId) {
    return !!this._users[userId] && this._users[userId].length > 0;
  }
}

export default new AppState();
