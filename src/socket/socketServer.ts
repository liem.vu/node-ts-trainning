import _ from 'lodash';
import { Server } from 'socket.io';
import SOCKET_AUTH from 'socketio-auth';
import SocketValidator from './socketValidator';
import AppState from './AppState';
import config from '../config/index';
import logger from '../loaders/logger';

const socketConfig = config.socket;

class SocketServer {
  public start = io => {
    const notificationSocketNspIo: Server = io.of(socketConfig.nsp_notification);
    console.log('socket', socketConfig.nsp_notification);
    
    this.startNotificationIo(notificationSocketNspIo);

    return [{ name: 'notificationSocket', ioNsp: notificationSocketNspIo }];
  };
  /**
   *
   * @param {*} notificationIo
   * Socket io for listener notification
   */
  private startNotificationIo = notificationIo => {
    /**
     *  Handler authentication
     */
    SOCKET_AUTH(notificationIo, {
      authenticate: (socket, data, callback) => {
        const { access_token } = data;
        console.log('access_token', access_token);
        

        SocketValidator.validateAuth(access_token)
          .then((user: any) => {
            logger.info(`Client ${user.id} - ${socket.id} authenticated`);
            socket.handshake.secure = user;
            AppState.addSocket(socket.id, user.id);
            return callback(null, true);
          })
          .catch(() => {
            return callback(null, false);
          });
      },
      timeout: 10000,
    });

    /**
     * Handler connection and event
     */
    notificationIo.on('connection', socket => {
      socket.on(socketConfig.event.notification, payload => {
        const { recipient_id } = payload;
        const userSocketIds = AppState.getSocketIds(recipient_id);
        _.forEach(userSocketIds, socketId => {
          notificationIo.to(socketId).emit(socketConfig.event.notification, payload);
        });
      });

      socket.on('disconnect', () => {
        AppState.removeSocket(socket.id, socket.handshake.secure.id);
      });

      socket.on('error', error => {
        logger.error(`Client ${error}`);
      });
    });
  };
}
export default new SocketServer();
