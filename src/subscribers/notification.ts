import EventEmitter from 'events';
import _ from 'lodash';
import { Container } from 'typedi';

import logger from '../loaders/logger';
import config from '../config/index';

import { Server } from 'socket.io';
import { UserService } from '../services/user';
import FirebaseAdmin from '../loaders/firebase';
const socketConfig = config.socket;

class NotificationEvent extends EventEmitter {}

const NotificationEmitter = new NotificationEvent();

NotificationEmitter.on('connect', () => {
  logger.info('✌️ Notification subscriber on ready!');
});

NotificationEmitter.on('push_notification_socket', (notifi: any) => {
  if (notifi) {
    const NotificationIoNsp: Server = Container.get('notificationSocket');
    console.log('notifi', notifi);
    
    NotificationIoNsp.emit(socketConfig.event.notification, notifi);
  }
});

NotificationEmitter.on('push_notification', (notifi: any) => {
  const userServiceInstance = Container.get(UserService);
  userServiceInstance
    .getUserById(notifi.recipient_id)
    .then(user => {
      if (!user.fcm_token) {
        return;
      }
      console.log('fcm', user.fcm_token);
      console.log('notifi', notifi);
      
      const message: FirebaseAdmin.messaging.Message = {
        token: user.fcm_token,
        notification: {
          title: notifi.title || '',
          body: notifi.content || '',
        },
        data: notifi.data ? notifi.data : {},
        // Set Android priority to "high"
        android: {
          priority: 'high',
        },
        // Add APNS (Apple) config
        apns: {
          payload: {
            aps: {
              contentAvailable: true,
            },
          },
          headers: {
            // 'apns-push-type': 'background',
            'apns-priority': '10',
          },
        },
      };
      console.log('message', message);

      return FirebaseAdmin.messaging()
        .send(message)
        .then(() => {
          console.log('sucess send firebase');
          
          // Do nothing
        });
    })
    .catch(err => {
      logger.error(`[NotificationSubscriber.push_notification]: Send notifi err: ` + err);
    });
});

export default NotificationEmitter;
