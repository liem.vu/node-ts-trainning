FROM node:alpine

WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
COPY .env .env
EXPOSE 3306
EXPOSE 3000
CMD ["npm", "start"]